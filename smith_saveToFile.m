function smith_saveToFile(seq1, seq2, aln1, aln2, bounds)
    % Function which is responsible for saving results of smith-waterman
    % algorith to a txt file
    char_aln1 = char(aln1);
    char_aln2 = char(aln2);
    len = length(char_aln1);
    
    connector = "";
    for i = 1 : len
        if(char_aln1(i) == char_aln2(i))
            connector = connector + "|";
        elseif(char_aln1(i) == "-" || char_aln2(i) == "-")
            connector = connector + " ";
        else
            connector = connector + " ";
        end
    end

    text = ">seq1: "  + "    " + bounds(1)  + newline +...
        aln1 + newline + ...
        ">seq2: " + "    " + bounds(2)  + newline + ...
        aln2
        %connector + newline + ...
        
    path = "E:\Nauka In�ynieria Biomedyczna\Semestr VI\WDB\Laboratorium\2\all\smith_waterman\test.txt";
    fid = fopen(path,'wt');
    fprintf(fid, text);
    fclose(fid);
   
end

