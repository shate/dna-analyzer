function draw(strMatrix)
    % For short sequences
    figure;
    pos = get(gcf, 'Position');
    width = pos(3);
   
    if(size(strMatrix, 1) >= size(strMatrix, 2))
        cellSize = width / size(strMatrix, 1);
    else
        cellSize = width / size(strMatrix, 2);
    end

    xlabel("First sequence");
    ylabel("Second sequence");
    title("")
    set(gca, 'visible', 'off')
    set(findall(gca, 'type', 'text'), 'visible', 'on')
    set(gca,'XAxisLocation','top');

    for j  = 1 : size(strMatrix, 1)
        for i = 1 : size(strMatrix, 2)
            txt = strMatrix(j, i);
            if(contains(txt,"*"))
                color = 'r';
                txt = extractBetween(txt,4,7);
                fontWeight = 'bold';
            else
                color = 'k';
                fontWeight = 'normal';
            end
            text(i * cellSize + cellSize / 4, (size(strMatrix, 2) - j + 1) * cellSize + cellSize/2, txt, 'FontSize', cellSize / 2.7, 'Color', color, 'FontWeight', fontWeight);
            rectangle('Position', [i * cellSize, (size(strMatrix, 2) - j + 1) * cellSize, cellSize, cellSize]);
        end
    end
end

