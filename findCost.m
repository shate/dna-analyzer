function findCost = findCost(nuc1, nuc2, M, gap)
    % Finds cost for a given nucleotides and substitution matrix
    row = 0;
    col = 0;
    for i = 1 : size(M, 1)
        if (M{i, 1} == nuc1)
            row = i;
        end
    end

    for j = 1 : size(M, 2)
        if (M{1, j} == nuc2)
            col = j;
        end
    end
    if(row == 0 || col == 0) % inny symbol podany (zdarzalo sie ze w sekwencjach z  NCBI pojawialy sie inne literki)
        findCost = gap;
    else
        findCost = M{row, col};
    end
end

