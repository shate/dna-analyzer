function star_global(sequences, M, gap, save)
% STAR_GLOBAL generates the output CLUSTAL and FASTA along with MSA matrix
% The first step is to get the lowest sequence cost and place it in the
% center
% The next step is to get the sequence with the lowest cost to the one
% previously chosen
% All other steps consist  selecting sequences with the lowest cost in
% relation to the central sequence and addng them to the MSA using
% addBreaks function
    seqNum = size(sequences, 2);

    %k = 2;
    %combinations = nchoosek(sequences, k);
    MSA = sequences;
      
    for i = 1 : seqNum
        MSA{1, i} = '';
    end
    costMatrix = zeros(seqNum, seqNum);
    alnMatrix = cell(seqNum, seqNum);
    sumCost = zeros(1, seqNum);
    costs = {sequences, sumCost, costMatrix, alnMatrix, MSA};
    
    for i = 1 : seqNum
        for j = 1 : seqNum
            seq1 = sequences{1, i};
            seq2 = sequences{1, j};
            
            [V, cost] = needleman_distance_Matrix(seq1, seq2, M, gap);
            [aln2, aln1] = needleman_distance_findPath(seq1, seq2, V, M, gap);
            
            
            costs{1, 2}(i) = costs{1, 2}(i) + cost;
            costs{1, 3}(i, j) = cost;
            costs{1, 4}{i, j} = [aln1, aln2];
        end
    end
        
    minIndex = 1;
    min = costs{1, 2}(1);
    for i = 1 : seqNum
        if(costs{1, 2}(i) < min )
            min = costs{1, 2}(i);
            minIndex = i;
        end
    end
    compatibleToMinIndex = 1;
    compMin = Inf;
    for j = 1 : seqNum
        if(costs{1, 3}(minIndex, j) < compMin && costs{1, 3}(minIndex, j) ~= 0)
            compMin = costs{1, 3}(minIndex, j);
            compatibleToMinIndex = j;
        end
    end
    minIndex
    compatibleToMinIndex
    MSA{1, 1} = costs{1, 4}{minIndex, compatibleToMinIndex}(1)
    MSA{1, 2} = costs{1, 4}{minIndex, compatibleToMinIndex}(2)
    
    MSA{2, 1} = sequences{2, minIndex};
    MSA{2, 2} = sequences{2, compatibleToMinIndex};
    
    
    
    seqLeft = seqNum - 2;
    alreadyMatched = [minIndex, compatibleToMinIndex];
     
    
    while(seqLeft > 0)
        tempMin = Inf;
        tempMinIndex = 0;
        for i = 1 : seqNum
           if(~ismember(i, alreadyMatched))
               if(costs{1, 3}(minIndex, i) < tempMin)
                   tempMin = costs{1, 3}(minIndex, i);
                   tempMinIndex = i;
               end
           end
        end
        seqLeft = seqLeft - 1;
        alreadyMatched(end + 1) = tempMinIndex;
        MSA = addBreaks(MSA, seqNum - seqLeft, minIndex, tempMinIndex, costs{1, 4});
        MSA{2, seqNum - seqLeft} = sequences{2, tempMinIndex};
    end
    summedCost = 0;
    for i = 1 : size(costMatrix, 1)
        for j = 1 : size(costMatrix, 2)
            if (i < j)
                summedCost = summedCost + costs{1, 3}(i, j);
            end
        end
    end
   
    if(save == 1)
        cost = costMSA(M, gap, MSA);
        bottom_bound = summedCost;
        top_bound = cost;
        star_saveToFile(MSA, bottom_bound, top_bound)
    end
end

