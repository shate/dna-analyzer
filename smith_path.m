function [aln1, aln2, pathMatrix, similarity, bounds] = smith_path(seq1, seq2, V, M, gap)
    % Generates pathMatrix, similarity value, aln1, aln2 and bounds of
    % changed sequences
    pathMatrix = zeros(size(V, 1), size(V, 2));
    aln1 = "";
    aln2 = "";
    maximum = max(V(:));
    [row, col] = find(ismember(V, maximum));
    similarity = maximum;

    if(size(row, 1) > 1)
        i = row(2, 1);
        j = col(2, 1);
    else
        i = row;
        j = col;
    end
    
    currentValue = maximum;
    bound1 = num2str(j - 1);
    bound2 = num2str(i - 1);
   
    while(currentValue > 0)
        if(i > 0 && j > 0 && (V(i, j) == (V(i - 1, j - 1) + findCost(seq1(j - 1), seq2(i - 1), M))))
            aln1 = seq1(j - 1) + aln1;
            aln2 = seq2(i - 1) + aln2;
            
            pathMatrix(i, j) = 1;
            currentValue = V(i - 1, j - 1);
            i = i - 1;
            j = j - 1;
        else
            if(j > 0 && (V(i, j) == (V(i - 1, j) + gap)))
                aln1 = "-" + aln1;
                aln2 = seq2(i - 1) + aln2;
                pathMatrix(i, j) = 1;
                currentValue = V(i - 1, j);
                i = i - 1;

            else
                aln1 = seq1(j - 1) + aln1;
                aln2 = "-" + aln2;
                pathMatrix(i, j) = 1;
                currentValue = V(i, j - 1);
                j = j - 1;

            end
        end
        
    end
    pathMatrix(i, j) = 1;
    bound1 = num2str(j) + " - " + bound1;
    bound2 = num2str(i) + " - " + bound2;
    bounds = [bound1, bound2];
end

