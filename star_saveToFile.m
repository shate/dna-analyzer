function star_saveToFile(MSA, bottom_bound, top_bound)
    iterator = 0;
    text = "";
    count = 0;
    correct = 0;
    while(iterator < length(MSA{1, 1}))
        numberOfElements = 60;
        if(iterator + numberOfElements > length(MSA{1, 1}))
            numberOfElements = length(MSA{1, 1}) - iterator;
        end
        for n = 1 : size(MSA, 2)
            text = text + MSA{2, n} + ": " + extractBetween(MSA{1, n}, iterator + 1, iterator + numberOfElements) + newline;
        end
        text = text + "  "; % dwie spacje za ": "
        for i = 1 : length(MSA{2, 1})
            text = text + " "; % po jednej spacji za kazda litere w ID sekwencji
        end
        
      
        for i = iterator + 1 : iterator + numberOfElements
            count = count+1;
            sameNucleotides = 0;
            for j = 1 : size(MSA, 2)
                if(strcmp(MSA{1, 1}(i), MSA{1, j}(i)))
                    sameNucleotides = sameNucleotides + 1;
                end
            end
            if(sameNucleotides == size(MSA, 2))
                text = text + "*";
                correct = correct + 1;
                
            else
                text = text + " ";
            end
        end
        text = text + newline + newline;
        iterator = iterator + 60;
    end
    
    saveText = "";
    for i = 1 : size(MSA, 2)
        saveText = strcat(saveText,"> ", MSA{2, i});
        saveText = saveText + newline;
        saveText = saveText + MSA{1, i};
        saveText = saveText + newline;
    end
    
    
    saveText = saveText + newline;
    text
    saveText = strcat(saveText, "Optimal cost range: [", num2str(bottom_bound), " - ", num2str(top_bound) + "]")
    
    path = "E:\Nauka In�ynieria Biomedyczna\Semestr VI\WDB\Laboratorium\dna-analyzer\star_txt\star.txt";
    fid = fopen(path,'wt');
    fprintf(fid, saveText);
    fclose(fid);
    
end

