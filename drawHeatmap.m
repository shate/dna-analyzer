function drawHeatmap(matrix, path)
    % Draws heatmap based on a matrix and a path
    for i = 1 : size(matrix, 1)
        for j = 1 : size(matrix, 2)
            if(path(i, j) == 1)
                matrix(i, j) = NaN;
            end
        end
    end

    h = heatmap(matrix, 'MissingDataColor', [0 0 0], 'GridVisible','off');
    hs = struct(h);
    hs.XAxis.TickValues = [];
    hs.YAxis.TickValues = [];
end

