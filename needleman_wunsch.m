function needleman_wunsch(seq1, seq2, M, gap, save, mode)
    % Function which is responsible for needleman-wunsch algorithm
    % (based on distance)
    if(mode == "distance")
        [V, ~] = needleman_distance_Matrix(seq1, seq2, M, gap);
        [aln2, aln1, path] = needleman_distance_findPath(seq1, seq2, V, M, gap);

    elseif(mode == "similarity")
        V = needleman_similarity_Matrix(seq1, seq2, M, gap);
        [aln2, aln1, path] = needleman_similarity_findPath(seq1, seq2, V, M, gap);

    end
    %{
    %For drawing shorter sequences
    matrix = generateStringMatrix(seq1, seq2, V, path);
    draw(matrix)
    figure;
    %}
    drawHeatmap(V, path);
    
    if(save == 1)
        needleman_saveToFile(seq1, seq2, M, gap, V, aln1, aln2, mode)
        savePlot();
    end
end

