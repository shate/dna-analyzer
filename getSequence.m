function sequence = getSequence(fasta)
    % Gets sequence out of fasta written file
    codeArray = strsplit(fasta, '\n');
    sequence = "";
    for i = 2 : length(codeArray)
        sequence = strcat(sequence, codeArray(i));
    end
end

