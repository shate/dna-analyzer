function MSA2 = addBreaks(MSA, index, firstIndex, currentIndex, alnMatrix)
% ADDBREAKS Adds breaks to the matching sequences. The breaks are added
% with the following rules:
% 1) Copy the breaks from current center sequence to the added sequence and
% to the  center sequence from the currently added sequences pair
% 2) Copy the breaks to all other sequences in the middle of MSA matrix
% 3) Set the center sequence as the center sequence from the currently
% added pair (with copied from point 1 breaks)
% 4) Set the last MSA sequence as the new added sequence (with copies from point 1 breaks)
    base_aln = char(MSA{1, 1});
    add_base_aln = char(alnMatrix{firstIndex, currentIndex}(1));
    add_aln = char(alnMatrix{firstIndex, currentIndex}(2));
    
  
    for i  = 1 : length(base_aln)
       if(strcmp(base_aln(i), '-') && ~strcmp(add_base_aln(i), '-'))
          add_aln = char(extractBetween(add_aln, 1, i - 1) + "-" + extractBetween(add_aln, i, length(add_aln)));
          add_base_aln = char(extractBetween(add_base_aln, 1, i - 1) + "-" + extractBetween(add_base_aln, i, length(add_base_aln)));
       end
    end  
    
    for k = 2 : index - 1
        for j = 1: length(add_base_aln)
            tempAln = char(MSA{1, k});
            if(strcmp(add_base_aln(j), '-') && ~strcmp(add_aln(j), '-'))
                if(j > length(tempAln))
                    tempAln = char(tempAln + "-");
                elseif(j <= length(base_aln) && ~strcmp(base_aln(j), '-'))
                    tempAln = char(extractBetween(tempAln, 1, j - 1) + "-" + extractBetween(tempAln, j, length(tempAln)));
                elseif(j > length(base_aln))
                    tempAln = char(extractBetween(tempAln, 1, j - 1) + "-" + extractBetween(tempAln, j, length(tempAln)));
                end
                MSA{1, k} = tempAln;
            end
        end
    end
    
    MSA{1, 1} = add_base_aln;
    MSA{1, index} = add_aln;
    for i = 1 : size(MSA, 2)
        MSA{1, i} = char(MSA{1, i});
    end
    MSA2 = MSA;
   
end
