function smith_waterman(seq1, seq2, M, gap, save)
    % Function communicates all other functions responsible for
    % smith-waterman algorithm
    
    V = smith_matrix(seq1, seq2, M, gap);
    [aln1, aln2, path, ~, bounds] = smith_path(seq1, seq2, V, M, gap);
    %{
    %For drawing shorter sequences
    matrix = generateStringMatrix(seq1, seq2, V, path);
    draw(matrix)
    figure;
    %}
    drawHeatmap(V, path);
    
    if(save == 1)
        smith_saveToFile(seq1, seq2, aln1, aln2, bounds)
        savePlot();
    end
end

