function sequence = getSeqFromNCBI(id)
    % Gets sequence from a NCBI website
    URL = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi';
    fasta = webread(URL, 'db', 'nucleotide',...
        'rettype', 'fasta',...
        'retmode', 'text',...
        'id', id);
    sequence = getSequence(fasta);
end

