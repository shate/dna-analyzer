function [aln2, aln1, pathMatrix] = needleman_similarity_findPath(seq1, seq2, V, M, gap)
% Generates two changed sequences based on the distanceMatrix (V) 
% and a pathMatrix which shows the path.
    pathMatrix = zeros(size(V, 1), size(V, 2));
    aln1 = "";
    aln2 = "";
    
    i = size(V, 1);
    j = size(V, 2);
    while(~(i == 1 && j == 1))
        
        if(i > 0 && j > 0 && (V(i, j) == (V(i - 1, j - 1) + findCost(seq1(j - 1), seq2(i - 1), M))))
            
            aln1 = seq1(j - 1) + aln1;
            aln2 = seq2(i - 1) + aln2;
            i = i - 1;
            j = j - 1;
            pathMatrix(i, j) = 1;
            if(i == 1)
                for k = j : -1 : 2
                    aln1 = seq1(j - 1) + aln1;
                    aln2 = "-" + aln2;
                    j = j - 1;
                    pathMatrix(i, j) = 1;
                end
            end
            
            if(j == 1)
                for k = i : -1 : 2
                    aln1 = "-" + aln1;
                    aln2 = seq2(i - 1) + aln2;
                    i = i - 1;
                    pathMatrix(i, j) = 1;
                end
            end
        else
            
            if(i > 0 && (V(i, j) == (V(i - 1, j) + gap)))
                
                aln1 = "-" + aln1;
                aln2 = seq2(i - 1) + aln2;
                i = i - 1;
                
                pathMatrix(i, j) = 1;
                
                if(i == 1)
                    for k = j : -1 : 2
                        aln1 = seq1(j - 1) + aln1;
                        aln2 = "-" + aln2;
                        j = j - 1;
                        pathMatrix(i, j) = 1;
                    end
                end

            elseif(j > 0 && V(i, j) == (V(i, j - 1) + gap))
                
                aln1 = seq1(j - 1) + aln1;
                aln2 = "-" + aln2;
                j = j - 1;
                pathMatrix(i, j) = 1;
                
                if(j == 1)
                    for k = i : -1 : 2
                        aln1 = "-" + aln1;
                        aln2 = seq2(i - 1) + aln2;
                        i = i - 1;
                        pathMatrix(i, j) = 1;
                    end
                end
            end
        end
    end
    pathMatrix(end, end) = 1;
end
