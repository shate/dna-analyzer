function analyze_many(penalties, method, save, varargin)
    % Function which gets info about sequences.
    % Varargin stands for cell arrays with sequence info. Single cell array
    % consists of : {sequenceInfo, methodInfo, sequenceID (used to shot output)}
    % Sequence info is: ncbi ID for ncbi method, filepath for file method, sequence for keyboard method)
    % methodInfo may be: 'keyboard', 'ncbi' or 'file'
    % IDs are the way you want to mark your sequences
    sequences = {zeros(2, length(varargin))};
    methods = {zeros(1, length(varargin))};
    
    for i = 1 : length(varargin)
        methods{i} = varargin{i}{2};
        if(strcmp(methods{i}, 'keyboard'))
            sequences{1, i} = char(varargin{i}{1});
        elseif(strcmp(methods{i}, 'ncbi'))
            sequences{1, i} = char(getSeqFromNCBI(str2double(varargin{i}{1})));
        elseif(strcmp(methods{i}, 'file'))
            sequences{1, i} = char(getSeqFromFile(varargin{i}{1}));
        end
        sequences{2, i} = varargin{i}{3};
    end
    
    M = penalties{1};
    gap = penalties{2};
    
    if(method == 'star_global')
        star_global(sequences, M, gap, save)
    end
end

