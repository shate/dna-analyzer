function cost = costMSA(M, gap, MSA)
    cost = 0;
    for i = 1 : length(MSA{1, 1})
        for j = 1 : size(MSA, 2)
            for k =  j + 1 : size(MSA, 2)
                if((strcmp(MSA{1, j}(i), '-') && ~strcmp(MSA{1, k}(i), '-')) || (~strcmp(MSA{1, j}(i), '-') && strcmp(MSA{1, k}(i), '-')))
                    cost = cost + gap;
                elseif(~strcmp(MSA{1, j}(i), MSA{1, k}(i)) && ~strcmp(MSA{1, j}(i), '-') && ~strcmp(MSA{1, k}(i), '-'))
                    cost = cost + findCost(MSA{1, j}(i), MSA{1, k}(i), M, gap);
                end
            end
        end
    end
end

