function [P, D, Q] = gotoh_local_matrix(seq2, seq1, M, gapEntry, gapContinue)
    % Generates the similarity matrix based on 'Gotoh (local)' algorithm
    % with a substitution matrix and gap penalty
    m = length(seq1);
    n = length(seq2);
    D = zeros(m + 1, n + 1);
    P = D;
    Q = D;
    for i = 0 : m
        D(i + 1, 1) =  0;
        P(i + 1, 1) = NaN;
        Q(i + 1, 1) = -Inf;
    end
    for j = 0 : n
        D(1, j + 1) =  0;
        P(1, j + 1) = -Inf;
        Q(1, j + 1) = NaN;
    end
        
    for i = 2 : m + 1
        for j = 2 : n + 1
            p_one = D(i - 1, j) + gapEntry + gapContinue;
            p_two = P(i - 1, j) + gapContinue;
            P(i, j) = max([p_one, p_two]);
            
            q_one = D(i, j - 1) + gapEntry + gapContinue;
            q_two = Q(i, j - 1) + gapContinue;
            Q(i, j) = max([q_one, q_two]);
            
            d_one = D(i - 1, j - 1) + findCost(seq1(i - 1), seq2(j - 1), M);
            d_two = P(i, j);
            d_three = Q(i, j);
            
            D(i, j) = max([d_one, d_two, d_three, 0]);
            
        end
    end
    P
    D
    Q
end

