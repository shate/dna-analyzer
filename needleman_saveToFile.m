function needleman_saveToFile(seq1, seq2, M, gap, V, aln1, aln2, method)
    match = M{2, 2};
    mismatch = M{2, 3};
    mode = method;
    score = V(size(V, 1), size(V, 2));
      
    char_aln1 = char(aln1);
    char_aln2 = char(aln2);
    len = length(char_aln1);

    identity = 0;
    gaps = 0;
    connector = "";

    for i = 1 : len
        if(char_aln1(i) == char_aln2(i))
            identity = identity + 1;
            connector = connector + "|";
        elseif(char_aln1(i) == "-" || char_aln2(i) == "-")
            gaps = gaps + 1;
            connector = connector + " ";
        else
            connector = connector + " ";
        end
    end

    gapsPercent = round(100 * gaps / len);
    identityPercent = round(100 * identity / len);


    text = "# 1: " + seq1 + newline + ...
        "# 2: " + seq2 + newline + ...
        "# Mode: " + mode + newline + ...
        "# Match: " + match + newline + ...
        "# Mismatch: " + mismatch + newline + ...
        "# Gap: " + gap + newline + ...
        "# Score: " + score + newline + ...
        "# Length: " + len + newline + ...
        "# Identity: " + identity + "/" + len + " (" + identityPercent + "%%)" + newline + ...
        "# Gaps: " + gaps + "/" + len + " (" + gapsPercent + "%%)" + newline + ...
        aln2 + newline + ...
        connector + newline + ...
        aln1

    path = "E:\Nauka In�ynieria Biomedyczna\Semestr VI\WDB\Laboratorium\2\all\needleman_wunsch\test.txt";
    fid = fopen(path,'wt');
    fprintf(fid, text);
    fclose(fid);
   
end

