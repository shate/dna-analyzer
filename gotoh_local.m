function gotoh_local(seq1, seq2, M, gapEntry, gapContinue, save)
    % Function which is responsible for gotoh (local) algorithm
    [P, D, Q] = gotoh_local_matrix(seq1, seq2, M, gapEntry, gapContinue);
    [path, ~, aln1, aln2, bounds] = gotoh_local_path(seq1, seq2, D, P, Q, M, gapEntry, gapContinue);
    %{
    %For drawing shorter sequences
    matrix = generateStringMatrix(seq1, seq2, V, path);
    draw(matrix)
    figure;
    %}
    drawHeatmap(D, path);
    
    if(save == 1)
        smith_saveToFile(seq1, seq2, aln1, aln2, bounds)
        savePlot();
    end
end

