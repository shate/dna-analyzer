function [pathMatrix, similarity, aln1, aln2, bounds] = gotoh_local_path(seq1, seq2, D, P, Q, M, gapEntry, gapContinue)
    % Generates pathMatrix, similarity value, aln1, aln2 and bounds of
    % changed sequences
    pathMatrix = zeros(size(D, 1), size(D, 2));
    aln1 = "";
    aln2 = "";
    maximum = max(D(:));
    [row, col] = find(ismember(D, maximum));
    similarity = maximum;

    if(size(row, 1) > 1)
        i = row(2, 1);
        j = col(2, 1);
    else
        i = row;
        j = col;
    end
    
    currentValue = maximum;
    bound1 = num2str(j - 1);
    bound2 = num2str(i - 1);
    
    while(currentValue > 0)
        if(i > 0 && j > 0 && (D(i, j) == (D(i - 1, j - 1) + findCost(seq1(j - 1), seq2(i - 1), M))))
            aln1 = seq1(j - 1) + aln1;
            aln2 = seq2(i - 1) + aln2;
            
            pathMatrix(i, j) = 1;
            currentValue = D(i - 1, j - 1);
            i = i - 1;
            j = j - 1;
        elseif(i > 0 && (D(i, j) == (D(i - 1, j) + gapEntry + gapContinue)))
            aln1 = "-" + aln1;
            aln2 = seq2(i - 1) + aln2;
            pathMatrix(i, j) = 1;
            currentValue = D(i - 1, j);
            i = i - 1;

        elseif(j > 0 && (D(i, j) == (D(i, j - 1) + gapEntry + gapContinue)))
            aln1 = seq1(j - 1) + aln1;
            aln2 = "-" + aln2;
            pathMatrix(i, j) = 1;
            currentValue = D(i, j - 1);
            j = j - 1;
        elseif(i > 0 && (P(i, j) == (P(i - 1, j) + gapContinue)))
            aln1 = "-" + aln1;
            aln2 = seq2(i - 1) + aln2;
            pathMatrix(i, j) = 1;
            currentValue = D(i - 1, j);
            i = i - 1;
        elseif(j > 0 && (Q(i, j) == (Q(i, j - 1) + gapContinue)))
            aln1 = seq1(j - 1) + aln1;
            aln2 = "-" + aln2;
            pathMatrix(i, j) = 1;
            currentValue = D(i, j - 1);
            j = j - 1;
        end
    end
   pathMatrix(i, j) = 1;
   bound1 = num2str(j) + " - " + bound1;
   bound2 = num2str(i) + " - " + bound2;
   bounds = [bound1, bound2];
end

