function V = smith_matrix(seq2, seq1, M, gap)
    % Generates the similarity matrix based on 'Smith and
    % Waterman' algorithm with a substitution matrix
    m = length(seq1);
    n = length(seq2);
    V = zeros(m + 1, n + 1);
    for i = 0 : m
        V(i + 1, 1) =  0;
    end
    for j = 0 : n
        V(1, j + 1) =  0;
    end
        
    for i = 2 : m + 1
        for j = 2 : n + 1
            cost = findCost(seq1(i - 1), seq2(j - 1), M);
            a = V(i - 1, j) + gap;  % deletion
            b = V(i, j - 1) + gap;  % insertion
            c = V(i - 1, j - 1) + cost;  % substitution
            temp = [a b c 0];
            V(i, j) = max(temp);
        end
    end
end

