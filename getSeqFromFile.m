function sequence = getSeqFromFile(fileName)
    % Gets sequence from a file
    text = textread(fileName, '%s');
    title = string(text(1, 1));
    code = string(text(2:end, 1));
    fasta = string(sprintf('%s\n%s', title, code));
    sequence = getSequence(fasta);
end

