function [V, distance] = needleman_distance_Matrix(seq2, seq1, M, gap)
% Generates the distance matrix based on 'Levenshtein distance'
% algorithm

    m = length(seq1);
    n = length(seq2);
    V = zeros(m + 1, n + 1);
    for i = 0 : m
        V(i + 1, 1) =  i * gap;
    end
    for j = 0 : n
        V(1, j + 1) =  j * gap;
    end
        
    for i = 2 : m + 1
        for j = 2 : n + 1
            cost = findCost(seq1(i - 1), seq2(j - 1), M, gap);
            
            a = V(i - 1, j) + gap;  % usuwanie
            b = V(i, j - 1) + gap;  % wstawianie
            c = V(i - 1, j - 1) + cost;  % zamiana
            temp = [a b c];
            V(i, j) = min(temp);
        end
    end
    distance = V(end, end);
end
