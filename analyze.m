function analyze(loadMethod1, loadValue1, loadMethod2, loadValue2, penalties, method, save)
    %{
    loadMethod1 - load method for the 1st sequence ('keyboard', 'file' or 'ncbi')
    loadValue1 - a value for chosen loadMethod1. Examples: 'keyboard': 'ACTGGC', 'file': 'filePath', 'ncbi': 'id')
    loadMethod2 - load method for the 2nd sequence ('keyboard', 'file' or 'ncbi')
    loadValue2 - a value for chosen loadMethod2. Examples: 'keyboard': 'ACTGGC', 'file': 'filePath', 'ncbi': 'id')
    penalties - cell array of penalties. Examples: [0, 1, 1] for match, mismatch, gap in needleman-wunsch or [M, -2] for smith-waterman, where M is substitution Matrix
    method - analyzing method. Examples: 'needleman_wunsch_dist', 'smith_waterman'
    save - 1 if yes, 0 if no
    %}
    if(strcmp(loadMethod1, 'keyboard'))
        sequence1 = loadValue1;
    elseif(strcmp(loadMethod1, 'file'))
        sequence1 = getSeqFromFile(loadValue1);
    elseif(strcmp(loadMethod1, 'ncbi'))
        sequence1 = getSeqFromNCBI(str2double(loadValue1));
    end
    
    if(strcmp(loadMethod2, 'keyboard'))
        sequence2 = loadValue2;
    elseif(strcmp(loadMethod2, 'file'))
        sequence2 = getSeqFromFile(loadValue2);
    elseif(strcmp(loadMethod1, 'ncbi'))
        sequence2 = getSeqFromNCBI(str2double(loadValue2));
    end
    
    seq1 = char(sequence1);
    seq2 = char(sequence2);
    M = penalties{1};
    gap = penalties{2};
    if (method == "needleman_wunsch_distance")
        
        needleman_wunsch(seq1, seq2, M, gap, save, "distance");
    elseif(method == "needleman_wunsch_similarity")
        
        needleman_wunsch(seq1, seq2, M, gap, save, "similarity");
    elseif (method == "smith_waterman")
        
        smith_waterman(seq1, seq2, M, gap, save);
    elseif (method == "gotoh_local")
        gapEntry = penalties{2};
        gapContinue = penalties{3};
        gotoh_local(seq1, seq2, M, gapEntry, gapContinue, save)
    end 
end

