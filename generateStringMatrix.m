function matrix = generateStringMatrix(seq1, seq2, V, path)
% Generates the string matrix with both sequences, costs
% and a path.
    char_seq1 = char(seq1);
    char_seq2 = char(seq2);
    matrix = repmat(" ", length(char_seq2) + 2, length(char_seq1) + 2);
   
    for i = 1 : length(char_seq2) + 2
        if(i == 1)
            matrix(i, 1) = ' ';
        elseif(i == 2)
            matrix(i, 1) = '-';
        else
            matrix(i, 1) = char_seq2(i - 2);
        end
    end

    for j = 1 : length(char_seq1) + 2
        if(j == 1)
            matrix(1, j) = ' ';
        elseif(j == 2)
            matrix(1, j) = '-';
        else
            matrix(1, j) = char_seq1(j - 2);
        end
    end

    for i = 2 : size(V, 1) + 1
        for j = 2 : size(V, 2) + 1
            matrix(i, j) = num2str(V(i - 1, j - 1));
            if(path(i - 1, j - 1) == 1)
                matrix(i, j) = strcat("*** ", matrix(i, j), " ***");
            end
        end
    end
end

