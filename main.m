%Script for testing
%% Needleman-Wunsch Distance Algorithm with values:
% Match = 0
% Mismatch = 1
% Gap = 1

M = {'#' 'A' 'C' 'G' 'T';
    'A' 0 1 1 1;
    'C' 1 0 1 1;
    'G' 1 1 0 1;
    'T' 1 1 1 0};

needleman_distance_penalties = {M, 1};

sequence1 = 'E:\Nauka In�ynieria Biomedyczna\Semestr VI\WDB\Laboratorium\dna-analyzer\star_txt\inne\1.txt';
sequence2 = 'E:\Nauka In�ynieria Biomedyczna\Semestr VI\WDB\Laboratorium\dna-analyzer\star_txt\inne\2.txt';

analyze('file', sequence1,...
    'file', sequence2,...
    needleman_distance_penalties, 'needleman_wunsch_distance', 1)

%% Needleman-Wunsch Similarity Algorithm with values:
% Match = 3
% Mismatch = -3
% Gap = -2

M = {'#' 'A' 'C' 'G' 'T';
    'A' 3 -3 -3 -3;
    'C' -3 3 -3 -3;
    'G' -3 -3 3 -3;
    'T' -3 -3 -3 3};
needleman_similarity_penalties = {M, -2}; % for needleman-wunsch similarity




analyze('ncbi', '1673171138',...
    'ncbi', '312152273',...
    needleman_similarity_penalties, 'needleman_wunsch_similarity', 1)

%% Smith-Waterman Algorithm with values:
% Match = 3
% Mismatch = -3
% Gap = -2
M = {'#' 'A' 'C' 'G' 'T';
    'A' 3 -3 -3 -3;
    'C' -3 3 -3 -3;
    'G' -3 -3 3 -3;
    'T' -3 -3 -3 3};

smith_penalties = {M, -2}; % for smith-waterman
analyze('keyboard', 'GACTCGATACGTGTGTGTCAGTCAGACGTCAGTCAGTACGTACGT',...
    'keyboard', 'GGTACGTATACGTACGACACGTACACCAGTACCAACCAGTCAGCACAAC',...
    smith_penalties, 'smith_waterman', 1)

%% Gotoh (Local) Algorithm (Gap penalties) for values: 
% Match = 3
% Mismatch = -3
% GapEntry = -3
% GapContinue = -1
M = {'#' 'A' 'C' 'G' 'T';
    'A' 3 -3 -3 -3;
    'C' -3 3 -3 -3;
    'G' -3 -3 3 -3;
    'T' -3 -3 -3 3};

gotoh_local_penalties = {M, -3, -1}; % for gotoh_local

analyze('keyboard', 'ACGATACGATACGATCAGACTAAAAAAAAAGACTACGACT',...
    'keyboard', 'GACTCGATACGTGTGTGTCAGTCAGACGTCAGTCAGTACGTACGT',...
    gotoh_local_penalties, 'gotoh_local', 1)

%% Star Algorithm (Many sequences)
M = {'#' 'A' 'C' 'G' 'T';
    'A' 0 1 1 1;
    'C' 1 0 1 1;
    'G' 1 1 0 1;
    'T' 1 1 1 0};

star_penalties = {M, 2};
%{
sequence1 = 'GCACAT';
sequence2 = 'TGAGA';
sequence3 = 'GACA';
sequence4 = 'GAACT';
%}

method1 = 'file';
method2 = 'file';
method3 = 'file';
method4 = 'file';
method5 = 'file';

sequence1 = 'E:\Nauka In�ynieria Biomedyczna\Semestr VI\WDB\Laboratorium\dna-analyzer\star_txt\inne\1.txt';
sequence2 = 'E:\Nauka In�ynieria Biomedyczna\Semestr VI\WDB\Laboratorium\dna-analyzer\star_txt\inne\2.txt';
sequence3 = 'E:\Nauka In�ynieria Biomedyczna\Semestr VI\WDB\Laboratorium\dna-analyzer\star_txt\inne\3.txt';
sequence4 = 'E:\Nauka In�ynieria Biomedyczna\Semestr VI\WDB\Laboratorium\dna-analyzer\star_txt\inne\4.txt';
sequence5 = 'E:\Nauka In�ynieria Biomedyczna\Semestr VI\WDB\Laboratorium\dna-analyzer\star_txt\inne\5.txt';


id1 = 'AB444587.1';
id2 = 'NG_048128.2';
id3 = 'NG_047664.1';
id4 = 'NG_047665.1';
id5 = 'NG_047368.1';
id6 = 'NZ_CP025210.1';

seqInfo1 = {sequence1, method1, id1};
seqInfo2 = {sequence2, method2, id2};
seqInfo3 = {sequence3, method3, id3};
seqInfo4 = {sequence4, method4, id4};
seqInfo5 = {sequence5, method5, id5};

analyze_many(star_penalties, 'star_global', 1, seqInfo1, seqInfo2, seqInfo3, seqInfo4, seqInfo5);
